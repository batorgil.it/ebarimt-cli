FROM frolvlad/alpine-glibc:alpine-3.15_glibc-2.34
# FROM alpine:latest
WORKDIR /app

RUN apk add --no-cache sqlite-libs
COPY ./lib/ ./lib/

RUN cp /usr/lib/libsqlite3.so.0 ./lib/ && \
  cp /usr/lib/libsqlite3.so.0.8.6 ./lib/ && \
  cp -r ./lib/lib* /usr/lib && \
  ln -s /usr/lib/libsqlite3.so.0.8.6  /usr/lib/libsqlite3.so && \
  ln -s /usr/lib/libicudata.so.53.1 /usr/lib/libicudata.so.53 && \
  ln -s /usr/lib/libicudata.so.53.1 /usr/lib/libicudata.so.5 && \
  ln -s /usr/lib/libicudata.so.53.1 /usr/lib/libicudata.so && \
  ln -s /usr/lib/libicui18n.so.53.1 /usr/lib/libicui18n.so.53 && \
  ln -s /usr/lib/libicui18n.so.53.1 /usr/lib/libicui18n.so.5 && \
  ln -s /usr/lib/libicui18n.so.53.1 /usr/lib/libicui18n.so && \
  ln -s /usr/lib/libicuuc.so.53.1 /usr/lib/libicuuc.so.53 && \
  ln -s /usr/lib/libicuuc.so.53.1 /usr/lib/libicuuc.so.5 && \
  ln -s /usr/lib/libicuuc.so.53.1 /usr/lib/libicuuc.so && \
  ln -s /usr/lib/libQt5Core.so.5.4.1 /usr/lib/libQt5Core.so.5.4 && \
  ln -s /usr/lib/libQt5Core.so.5.4.1 /usr/lib/libQt5Core.so.5 && \
  ln -s /usr/lib/libQt5Core.so.5.4.1 /usr/lib/libQt5Core.so && \
  ln -s /usr/lib/libQt5Network.so.5.4.1 /usr/lib/libQt5Network.so.5.4 && \
  ln -s /usr/lib/libQt5Network.so.5.4.1 /usr/lib/libQt5Network.so.5 && \
  ln -s /usr/lib/libQt5Network.so.5.4.1 /usr/lib/libQt5Network.so && \
  ln -s /usr/lib/libQt5Script.so.5.4.1 /usr/lib/libQt5Script.so.5.4 && \
  ln -s /usr/lib/libQt5Script.so.5.4.1 /usr/lib/libQt5Script.so.5 && \
  ln -s /usr/lib/libQt5Script.so.5.4.1 /usr/lib/libQt5Script.so && \
  ln -s /usr/lib/libQt5Sql.so.5.4.1 /usr/lib/libQt5Sql.so.5.4 && \
  ln -s /usr/lib/libQt5Sql.so.5.4.1 /usr/lib/libQt5Sql.so.5 && \
  ln -s /usr/lib/libQt5Sql.so.5.4.1 /usr/lib/libQt5Sql.so


RUN apk add openssl-dev g++ glib-dev

COPY ./ebarimt-cli /bin/ebarimt-cli
COPY ./ebarimt /bin/ebarimt